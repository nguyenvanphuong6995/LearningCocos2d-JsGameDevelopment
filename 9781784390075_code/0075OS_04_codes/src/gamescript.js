var level = [
     [1,1,1,1,1,1,1],
     [1,1,0,0,0,0,1],
     [1,1,3,0,2,0,1],
     [1,0,0,4,0,0,1],
     [1,0,3,1,2,0,1],
     [1,0,0,1,1,1,1],
     [1,1,1,1,1,1,1]
];

var cratesArray = [];
var playerPosition;
var playerSprite;
var startTouch;
var endTouch;
var swipeTolerance = 10;




var gameScene  = cc.Scene.extend({
	onEnter:function () {
		this._super();
		gameLayer = new game();
		gameLayer.init();
		this.addChild(gameLayer);
	}
});

var game  = cc.Layer.extend({
	init:function () {
		this._super();
		cache = cc.spriteFrameCache;
		cache.addSpriteFrames("assets/spritesheet.plist", "assets/spritesheet.png");
		var backgroundSprite = cc.Sprite.create(cache.getSpriteFrame("background.png"));
		backgroundSprite.getTexture().setAliasTexParameters();  
		backgroundSprite.setPosition(240,160);
		backgroundSprite.setScale(5);
		this.addChild(backgroundSprite);
		var levelSprite =  cc.Sprite.create(cache.getSpriteFrame("level.png"));
		levelSprite.setPosition(240,110);
		levelSprite.setScale(5);
		this.addChild(levelSprite);
		for(i=0;i<7;i++){
			cratesArray[i]=[];
			for(j=0;j<7;j++){
				switch(level[i][j]){
					case 4:
					case 6:
						playerSprite = cc.Sprite.create(cache.getSpriteFrame("player.png"));
						playerSprite.setPosition(165+25*j,185-25*i);
						playerSprite.setScale(5);
						this.addChild(playerSprite);
						playerPosition = {x:j,y:i};
						cratesArray[i][j]=null;
						break;
					case 3:
					case 5:
						var crateSprite = cc.Sprite.create(cache.getSpriteFrame("crate.png"));
						crateSprite.setPosition(165+25*j,185-25*i);
						crateSprite.setScale(5);
						this.addChild(crateSprite);
						cratesArray[i][j]=crateSprite;
						break;
				default:
					cratesArray[i][j]=null;
				} 
			} 
		} 
		cc.eventManager.addListener(listener, this);   
     }
}); 

var listener = cc.EventListener.create({
	event: cc.EventListener.TOUCH_ONE_BY_ONE,
     swallowTouches: true,
     onTouchBegan:function (touch,event) {
		startTouch = touch.getLocation();
		return true;
     },
	onTouchEnded:function(touch, event){
		endTouch = touch.getLocation();
		swipeDirection();          
     }
});

function swipeDirection(){
	var distX = startTouch.x - endTouch.x;
	var distY = startTouch.y - endTouch.y;
	if(Math.abs(distX)+Math.abs(distY)>swipeTolerance){
		if(Math.abs(distX)>Math.abs(distY)){
			if(distX>0){
				move(-1,0);
			}
			else{
				move(1,0);
			}
		}
		else{
			if(distY>0){
				move(0,1);
			}
			else{
				move(0,-1);
			}	
		}
	}
}

function move(deltaX,deltaY){
	switch(level[playerPosition.y+deltaY][playerPosition.x+deltaX]){
		case 0:
		case 2:
			level[playerPosition.y][playerPosition.x]-=4;
			playerPosition.x+=deltaX;
			playerPosition.y+=deltaY;
			level[playerPosition.y][playerPosition.x]+=4;
			playerSprite.setPosition(165+25*playerPosition.x,185-25*playerPosition.y);
			break;
		case 3:
		case 5: 
			if(level[playerPosition.y+deltaY*2][playerPosition.x+deltaX*2]==0 || level[playerPosition.y+deltaY*2][playerPosition.x+deltaX*2]==2){
				level[playerPosition.y][playerPosition.x]-=4;
				playerPosition.x+=deltaX;
				playerPosition.y+=deltaY;
				level[playerPosition.y][playerPosition.x]+=1;
				playerSprite.setPosition(165+25*playerPosition.x,185-25*playerPosition.y);	
				level[playerPosition.y+deltaY][playerPosition.x+deltaX]+=3;
				var movingCrate = cratesArray[playerPosition.y][playerPosition.x];
				movingCrate.setPosition(movingCrate.getPosition().x+25*deltaX,movingCrate.getPosition().y-25*deltaY);
				cratesArray[playerPosition.y+deltaY][playerPosition.x+deltaX]=movingCrate;
				cratesArray[playerPosition.y][playerPosition.x]=null;	
			}
			break;
	}
}