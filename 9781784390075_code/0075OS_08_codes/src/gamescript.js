var world;
var shapeArray=[];

var gameScene = cc.Scene.extend({
	onEnter:function () {
		this._super();
		gameLayer = new game();
		gameLayer.init();
		this.addChild(gameLayer);
	}
});

var game = cc.Layer.extend({
	init:function () {
		this._super();
		var backgroundLayer = cc.LayerGradient.create(cc.color(0xdf,0x9f,0x83,255), cc.color(0xfa,0xf7,0x9f,255));
		this.addChild(backgroundLayer); 
		world = new cp.Space();
		world.gravity = cp.v(0, -100);
		var debugDraw = cc.PhysicsDebugNode.create(world);
		debugDraw.setVisible(true);
		this.addChild(debugDraw);
		this.addBody(240,10,480,20,false,"assets/ground.png","ground");
		this.addBody(204,32,24,24,true,"assets/brick1x1.png","destroyable");
		this.addBody(276,32,24,24,true,"assets/brick1x1.png","destroyable");
		this.addBody(240,56,96,24,true,"assets/brick4x1.png","destroyable");
		this.addBody(240,80,48,24,true,"assets/brick2x1.png","solid");
		this.addBody(228,104,72,24,true,"assets/brick3x1.png","destroyable");
		this.addBody(240,140,96,48,true,"assets/brick4x2.png","solid");
		this.addBody(240,188,24,48,true,"assets/totem.png","totem"); 
		this.scheduleUpdate(); 
		cc.eventManager.addListener(touchListener, this);
		world.setDefaultCollisionHandler(this.collisionBegin,null,null,null);  		
	},
	addBody: function(posX,posY,width,height,isDynamic,spriteImage,type){
		if(isDynamic){
			var body = new cp.Body(1,cp.momentForBox(1,width,height));
		}
		else{
			var body = new cp.Body(Infinity,Infinity);
		}
		body.setPos(cp.v(posX,posY));
		var bodySprite = cc.Sprite.create(spriteImage);
          gameLayer.addChild(bodySprite,0);
          bodySprite.setPosition(posX,posY);
		if(isDynamic){		
		   world.addBody(body);		
		}
		var shape = new cp.BoxShape(body, width, height);
		shape.setFriction(1);
		shape.setElasticity(0);
		shape.name=type;
		shape.image=bodySprite;
		world.addShape(shape);
		shapeArray.push(shape);	
	},
	update:function(dt){
     	world.step(dt);
          for(var i=shapeArray.length-1;i>=0;i--){
               shapeArray[i].image.x=shapeArray[i].body.p.x
               shapeArray[i].image.y=shapeArray[i].body.p.y
               var angle = Math.atan2(-shapeArray[i].body.rot.y,shapeArray[i].body.rot.x);
               shapeArray[i].image.rotation= angle*57.2957795;
          }     
     },
     collisionBegin : function (arbiter, space ) {
          if((arbiter.a.name=="totem" && arbiter.b.name=="ground") || (arbiter.b.name=="totem" && arbiter.a.name=="ground")){
               console.log("Oh no!!!!");
          }
          return true;
     }
});

var touchListener = cc.EventListener.create({
     event: cc.EventListener.TOUCH_ONE_BY_ONE,
     onTouchBegan: function (touch, event) { 
          for(var i=shapeArray.length-1;i>=0;i--){
               if(shapeArray[i].pointQuery(cp.v(touch._point.x,touch._point.y))!=undefined){
                    if(shapeArray[i].name=="destroyable"){
                         gameLayer.removeChild(shapeArray[i].image);
                         world.removeBody(shapeArray[i].getBody())
                         world.removeShape(shapeArray[i])
                         shapeArray.splice(i,1);
                    }
               }
          }
     }
}) 